import csv
from parameterized import parameterized
import json 
class ReadCSVFile:
    # open csv file method
    def csvFileOpen(self):
        # open csv file
        fileOpen = open("apiautomation.csv")
        # read csv file
        csvRead = csv.reader(fileOpen)
        # convert in list csv file data
        listofcsv = list(csvRead)
        # close the file
        fileOpen.close()
        # return csv file data excluding the header
        return listofcsv[1:]

# call csv file data's method
listofcsv = ReadCSVFile().csvFileOpen()

projectName = listofcsv[0][0]
databaseName = listofcsv[0][1] 
table = json.loads(listofcsv[0][2])
serializer = eval(listofcsv[0][3])
api = eval(listofcsv[0][4])

# Extracting serializer class names
serializer_class_names = [entry["serializerClassName"] for entry in serializer]

# Joining the class names into a comma-separated string
importClasses = ", ".join(serializer_class_names)
