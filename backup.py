fastapi_code = f"""
#default imports 
from fastapi import FastAPI, Depends, HTTPException
import time

# custom file imports
import models
from database import db_dependecy
from serializers import  {serializer["serializerClassName"]}, UpdateUserBase

# create fastapi object
app = FastAPI()

# create the table
models.create_tables()

# create post api
@app.post("/users", status_code=201)
def addUser(users: UserBase, db: db_dependecy):
    usersData = models.User(name=users.name, email=users.email, password=users.password)
    db.add(usersData)
    db.commit()
    return {"message": "user added successfully"}

# get specific user details api
@app.get("/users/userId")
def getUser(userId: int, db: db_dependecy):
    data = db.query(models.User).filter(models.User.id == userId).first()
    if not data:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        return data

# get all user details api
@app.get("/users")
def getAllUser(db: db_dependecy):
    data = db.query(models.{table['apiModelClassName']}).all()
    return data

# get deleted user apis
@app.delete("/users/userId", status_code=204)
def deleteUser(userId: int, db: db_dependecy):
    data = db.query(models.User).filter(models.User.id == userId).first()
    if not data:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        db.delete(data)
        db.commit()
        return {"message": "User deleted successfully"}

# get update user details apis
@app.put("/users/userId", status_code=201)
def updateUser(userId: int, db: db_dependecy, users: UpdateUserBase):
    getdata = db.query(models.User).filter(models.User.id == userId).first()
    if not getdata:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        if users.name is None and users.email is None:
            raise HTTPException(status_code=401, detail="At least one field is required")
        else:
            if users.name:
                getdata.name = users.name
            if users.email:
                getdata.email = users.email
            if users.password:
                getdata.password = users.password
            db.commit()
            return {"message": "user details updated successfully"}
"""