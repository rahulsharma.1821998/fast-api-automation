import os
from csvrealtedcode import *

fastapi_code = f"""
from fastapi import FastAPI, Depends, HTTPException
import time

import models
from database import db_dependecy
from serializers import  {importClasses}

app = FastAPI()

# create the table
models.create_tables()
"""

for values in api:
    if values['apiMethodName'] == "get":
        fastapi_code += f"""
# get all user details api
@app.get("{values['apiPath']}")
def getAllUser(db: db_dependecy):
    data = db.query(models.{table["apiModelClassName"]}).all()
    return data
        """
    if values['apiMethodName'] == "post":
        requestFieldDatabaseField = ', '.join([f"{key}={values['requstcolumnName']}.{key}" for key in table['fields'].keys() & values['request_filedname']])
        fastapi_code += f"""
@app.post("{values['apiPath']}", status_code={values['statusCode']})
def addUser({values['requstcolumnName']}: {values['serializerName']}, db: db_dependecy):
    usersData = models.{table["apiModelClassName"]}({requestFieldDatabaseField})
    db.add(usersData)
    db.commit()
    return {values['apiResponse']}
        """
    if values['apiMethodName'] == "get<id>":
        fastapi_code += f"""
# get specific user details api
@app.get("{values['apiPath']}/{values['urlparam']}")
def getUser({values['urlparam']}: int, db: db_dependecy):
    data = db.query(models.User).filter(models.{table["apiModelClassName"]}.{values['urlparam']} == {values['urlparam']}).first()
    if not data:
        raise HTTPException(status_code={values['apiError']['statusCode']}, detail="{values['apiError']['message']}")
    else:
        return data
    """
    if values['apiMethodName'] == "delete":
        fastapi_code += f"""
# get deleted user apis
@app.delete("{values['apiPath']}/{values['urlparam']}", status_code={values['statusCode']})
def deleteUser({values['urlparam']}: int, db: db_dependecy):
    data = db.query(models.{table["apiModelClassName"]}).filter(models.{table["apiModelClassName"]}.{values['urlparam']} == {values['urlparam']}).first()
    if not data:
        raise HTTPException({values['apiError']['statusCode']}, detail="{values['apiError']['message']}")
    else:
        db.delete(data)
        db.commit()
        return {values['apiResponse']} 
        """
    if values['apiMethodName'] == "put":
        fastapi_code += f"""
        # get update user details apis
@app.put("{values['apiPath']}/{values['urlparam']}", status_code={values['statusCode']})
def updateUser({values['urlparam']}: int, db: db_dependecy, {values['requstcolumnName']}: {values['serializerName']}):
    getdata = db.query(models.{table["apiModelClassName"]}).filter(models.{table["apiModelClassName"]}.{values['urlparam']} == {values['urlparam']}).first()
    if not getdata:
        raise HTTPException({values['apiError']['statusCode']}, detail="{values['apiError']['message']}")
    else:
        if {values['requstcolumnName']}.name is None and {values['requstcolumnName']}.email is None:
            raise HTTPException({values['apiBadRequestError']['statusCode']}, detail="{values['apiBadRequestError']['message']}")
        else:
            if {values['requstcolumnName']}.name:
                getdata.name = {values['requstcolumnName']}.name
            if {values['requstcolumnName']}.email:
                getdata.email = {values['requstcolumnName']}.email
            if {values['requstcolumnName']}.password:
                getdata.password = {values['requstcolumnName']}.password
            db.commit()
            return {values['apiResponse']} 
        """

path = f"./{projectName}"
if os.path.exists(path):
    print("folder is already exist")
else:
    os.mkdir(path)
    # Writing to file
    with open(f"{path}/database.py", "w") as file1:
        import1 = "from sqlalchemy import create_engine \n"
        import2 = "from sqlalchemy import create_engine \n"
        import3 = "from sqlalchemy.orm import sessionmaker \n"
        import4 = "from sqlalchemy.ext.declarative import declarative_base \n"
        import5 = "from typing import List,Annotated \n"
        import6 = "from sqlalchemy.orm import Session \n"
        import7 = "from  fastapi import Depends \n"
        
        databaseurl = f"databaseurl = 'postgresql://postgres:root@localhost:5432/{databaseName}'\n"
        enginer = 'engine = create_engine(databaseurl) \n'
        sessionLocal = 'sessionLocal = sessionmaker(autocommit=False,autoflush=False,bind=engine) \n'
        Base = 'Base = declarative_base() \n'
        getDb = """def get_db():
    db = sessionLocal()  
    try:
        yield db
    finally:
        db.close()

"""
        db_dependecy = "db_dependecy = Annotated[Session,Depends(get_db)]"
        
        file1.writelines([import1,import2,import3,import4,import5,import6,import7, databaseurl, enginer, sessionLocal, Base, getDb, db_dependecy])
    # Writing to file
    with open(f"{path}/main.py", "w") as file1:
        # Writing data to a file
        file1.write(fastapi_code)
    
    # Writing to file
    with open(f"{path}/serializers.py", "w") as file:
        file.write("# serializers.py\n")
        file.write("from pydantic import BaseModel\n\n")

        code = ""
        for item in serializer:
            class_name = item["serializerClassName"]
            fields = item["fields"]
            
            code += f"class {class_name}(BaseModel):\n"
            
            for field, field_type in fields.items():
                code += f"    {field}: {field_type}\n"
            
            code += "\n"
        file.write(code)
    
    # Write the SQLAlchemy model and create_tables function to models.py
    with open(f"{path}/models.py", "w") as file1:
        file1.write("from sqlalchemy import Boolean, Integer, Column, ForeignKey, String\n")
        file1.write("from database import Base\n")
        file1.write("from database import engine\n\n")
        file1.write(f"class {table['apiModelClassName']}(Base):\n")
        file1.write(f"    __tablename__ = '{table['tableName']}'\n")
        for key, value in table['fields'].items():
            file1.write(f"    {key} = {value}\n")
        file1.write("def create_tables():\n")
        file1.write("    Base.metadata.create_all(bind=engine)\n")
